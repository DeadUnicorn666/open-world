from django.shortcuts import render
from django.views.generic import TemplateView
from open_world_app.models import ContactRequest
from django.http import HttpResponse, JsonResponse
from open_world_app.models import Document, Event, EventPhotos
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from json import dumps
from dateutil.parser import parse
from datetime import date
from calendar import monthrange


class MainPage(TemplateView):
    template_name = 'main.html'

    def post(self, request):
        cr = ContactRequest(name=request.POST.get(),
                            email=request.POST.get(),
                            phone_number=request.POST.get(),
                            comment=request.POST.get())
        cr.save()
        return HttpResponse("Hurray!")


class CalendarPage(TemplateView):
    template_name = 'calendar.html'

    @staticmethod
    def parse_events(events):
        event_list = []
        for event in events:
            image_urls = list(map(lambda x: x.image.url, EventPhotos.objects.filter(event=event)))
            event_list.append(dict(name=event.name,
                                   date=date.strftime(event.date, "%d %B, %Y"),
                                   description='&nbsp;' * 4 + event.description.replace('\n\r', '<br>' + '&nbsp;' * 4),
                                   image_urls=image_urls))
        return event_list

    @staticmethod
    def get_month_events(today):
        events = Event.objects.filter(date__year=today.year, date__month=today.month)
        return list(map(lambda event: event.date.day, events)), list(map(lambda event: event.name, events))

    @method_decorator(csrf_exempt)
    def post(self, request, **kwargs):
        event_date = request.POST.get('date')
        request_type = request.POST.get("type")
        if not event_date:
            return HttpResponse("Error")

        event_date = parse(event_date[:event_date.find('(')]).date()
        if request_type == 'event':
            events = self.parse_events(Event.objects.filter(date=event_date))
            return JsonResponse(dict(
                event=events[0],
            ))
        elif request_type == "busy_days":
            event_days, event_names = self.get_month_events(event_date)
            return JsonResponse(dict(
                busy_days=event_days,
                event_names=event_names
            ))


class ContactPage(TemplateView):
    template_name = 'contacts.html'

    def post(self, request, **kwargs):
        cr = ContactRequest(name=request.POST.get(),
                            email=request.POST.get(),
                            phone_number=request.POST.get(),
                            comment=request.POST.get())
        cr.save()
        return HttpResponse("Hurray!")


class DocumentPage(TemplateView):
    template_name = 'documents.html'

    def get(self, request, **kwargs):
        docs = Document.objects.values()
        print(docs)
        return render(request, self.template_name, context={'docs': docs})
