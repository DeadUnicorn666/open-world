from django.db import models
from open_world import settings
from django.core.exceptions import ValidationError


class ContactRequest(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    phone_number = models.CharField(max_length=35, null=True)
    comment = models.TextField(null=True)


class Document(models.Model):
    filename = models.CharField(max_length=100)


def validate_ids(value):
    try:
        list(map(int, value.split(',')))
    except:
        raise ValidationError("Value must be comma separated integer numbers.")


class Event(models.Model):
    date = models.DateField()
    name = models.CharField(max_length=100)
    description = models.TextField()
    # photos = models.CharField(max_length=200, validators=[validate_ids])


class EventPhotos(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images')
