from django.contrib import admin
from open_world_app.models import Document, ContactRequest, Event, EventPhotos

admin.site.register(Document)
admin.site.register(ContactRequest)
admin.site.register(Event)
admin.site.register(EventPhotos)
