from django.apps import AppConfig


class OpenWorldAppConfig(AppConfig):
    name = 'open_world_app'
