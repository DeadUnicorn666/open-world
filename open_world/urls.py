"""open_world URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from open_world_app import views
from open_world import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
import os


dir_patterns = []
folders = [d for d in os.listdir(settings.TEMPLATE_FOLDER) if os.path.isdir(settings.TEMPLATE_FOLDER + d)]
for folder in folders:
    for template in os.listdir(settings.TEMPLATE_FOLDER + folder):
        dir_patterns.append(
            path(f'{folder}/{template[:-5]}', TemplateView.as_view(template_name=f'{folder}/{template}'), name=f'{folder}_{template[:-5]}')
        )

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.MainPage.as_view(), name='main'),
    path('calendar/', csrf_exempt(views.CalendarPage.as_view()), name='calendar'),
    path('docs/', views.DocumentPage.as_view(), name='documents'),
    path('about/', TemplateView.as_view(template_name='about.html'), name='about'),
    path('contacts/', views.ContactPage.as_view(), name='contacts'),
    path('social/', TemplateView.as_view(template_name='social_help.html'), name='social'),
    path('law_help/', TemplateView.as_view(template_name='law_help.html'), name='law_help'),
    path('tourism/', TemplateView.as_view(template_name='tourism.html'), name='tourism'),
    path('sport/', TemplateView.as_view(template_name='sport_activity.html'), name='sport_activity'),
    path('donation/', TemplateView.as_view(template_name='donation.html'), name='donation'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + dir_patterns
